﻿using System.Threading.Tasks;

using PL.Domain.Core;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;
using PL.Domain.Entities;
using PL.Domain.Interfaces.DapperRepository;
using PL.Domain.Interfaces.Repositories;
using PL.Domain.Interfaces.Services;

namespace PL.Domain.Services {
	public class DeveloperService : ServiceSimple<Developer>, IDeveloperService {

        private readonly IDeveloperDapperRepository _repo;

        public DeveloperService(IDeveloperRepository repository, IDeveloperDapperRepository dapperRepository) : 
			base(repository) =>
            _repo = dapperRepository;

		public bool ExistsByDocument(string cpf) =>
			_repo.ExistsByDocument(cpf);

		public Task<IPagedResult<DeveloperDTO>> List(DatatableParameter parameter) => 
			_repo.ListAll(parameter);

		public DeveloperDTO GetByName(string name) => 
			_repo.GetByName(name);
	}
}
