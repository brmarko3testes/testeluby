﻿using System;
using System.Threading.Tasks;

using PL.Domain.Core;
using PL.Domain.Core.Pagination;
using PL.Domain.DTO;
using PL.Domain.Entities;
using PL.Domain.Interfaces.DapperRepository;
using PL.Domain.Interfaces.Repositories;
using PL.Domain.Interfaces.Services;

namespace PL.Domain.Services {
	public class DeveloperProjectService : ServiceSimple<DeveloperProject>, IDeveloperProjectService {

        private readonly IDeveloperProjectDapperRepository _repo;

        public DeveloperProjectService(IDeveloperProjectRepository repository, IDeveloperProjectDapperRepository dapperRepository) : 
			base(repository) =>
            _repo = dapperRepository;

		public Task<IPagedResult<DeveloperProjectDTO>> ListByProjectId(Guid projectId) =>
			_repo.GetByProject(projectId);
		public Task<IPagedResult<DeveloperProjectDTO>> ListByDeveloperId(Guid developerId) =>
			_repo.GetByDeveloper(developerId);
		public bool ExistsByName(string name) =>
			_repo.ExistsByName(name);
		public DeveloperProjectDTO GetByName(string name) =>
			_repo.GetByName(name);
	}
}
