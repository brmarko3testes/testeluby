﻿using System.Threading.Tasks;

using PL.Domain.Core;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;
using PL.Domain.Entities;
using PL.Domain.Interfaces.DapperRepository;
using PL.Domain.Interfaces.Repositories;
using PL.Domain.Interfaces.Services;

namespace PL.Domain.Services {
	public class ProjectService : ServiceSimple<Project>, IProjectService {

		private readonly IProjectDapperRepository _repo;

		public ProjectService(IProjectRepository repository, IProjectDapperRepository dapperRepository) :
			base(repository) =>
			_repo = dapperRepository;

		public Task<IPagedResult<ProjectDTO>> ListAll(DatatableParameter parameter) =>
			_repo.ListAll(parameter);

		public ProjectDTO GetByName(string name) =>
			_repo.GetByName(name);
	}
}
