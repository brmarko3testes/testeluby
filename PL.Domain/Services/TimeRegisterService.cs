﻿using System;
using System.Threading.Tasks;

using PL.Domain.Core;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;
using PL.Domain.Entities;
using PL.Domain.Interfaces.DapperRepository;
using PL.Domain.Interfaces.Repositories;
using PL.Domain.Interfaces.Services;

namespace PL.Domain.Services {
	public class TimeRegisterService : ServiceSimple<TimeRegister>, ITimeRegisterService {

        private readonly ITimeRegisterDapperRepository _repo;

        public TimeRegisterService(ITimeRegisterRepository repository, ITimeRegisterDapperRepository dapperRepository) : 
			base(repository) =>
            _repo = dapperRepository;

		public Task<IPagedResult<TimeRegisterDTO>> List(DatatableParameter parameter) =>
			_repo.ListAll(parameter);
	}
}
