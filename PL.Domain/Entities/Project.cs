﻿using System;
using System.Collections.Generic;

using PL.Domain.Core;
using PL.Domain.Core.Interfaces;

namespace PL.Domain.Entities {
	public class Project : GuidHandler, ISelfValidation {

		protected Project() { /* Empty private constructor */ }

		public Project(string name, DateTime startDate, DateTime endDate) {
			Id = NewId;
			Name = name;
			StartDate = startDate;
			EndDate = endDate;
		}

		public Guid Id { get; private set; }
		public string Name { get; private set; }

		public DateTime StartDate { get; private set; }
		public DateTime EndDate { get; private set; }

		public ValidationResult ValidationResult { get; private set; }
		public virtual ICollection<DeveloperProject> DevelopersInProject { get; private set; }

		public Project Edit(string projectName, DateTime startTime, DateTime endTime, DeveloperProject dev = null) {
			Edit(projectName, startTime, endTime);

			if (dev != null) {
				NewDeveloperProjectIfNull();
				DevelopersInProject.Add(dev);
			}
			return this;
		}
		public Project Edit(string projectName, DateTime startDate, DateTime endDate, ICollection<DeveloperProject> devs = null) {
			Edit(projectName, startDate, endDate);

			if (devs != null) {
				NewDeveloperProjectIfNull();
				((List<DeveloperProject>) DevelopersInProject).AddRange(devs);
			}
			return this;
		}

		public void Edit(string projectName, DateTime startDate, DateTime endDate) {
			Name = projectName;
			StartDate = startDate;
			EndDate = endDate;
		}

		private void NewDeveloperProjectIfNull() =>
			DevelopersInProject ??=
				new List<DeveloperProject>();

		public bool IsValid => true; // TODO criar as validaçõess
	}
}
