﻿using System;

using PL.Domain.Core;
using PL.Domain.Core.Interfaces;

namespace PL.Domain.Entities
{
	public class Developer : GuidHandler, ISelfValidation
	{
		protected Developer() { /* Empty private constructor */ }

		public Developer(string userName, string password, string email, string cpf) {
			Id = NewId;
			UserName = userName;
			Password = password;
			Email = email;
			CPF = cpf;
		}

		public Guid Id { get; private set; }
		public string UserName { get; private set; }
		public string Password { get; private set; }
		public string Email { get; private set; }

		// TODO transformar em um objeto do tipo Documento que terá validação específica.
		public string CPF { get; private set; }

		public ValidationResult ValidationResult { get; private set; }
		public bool IsValid => true; // TODO criar as validações
	}
}
