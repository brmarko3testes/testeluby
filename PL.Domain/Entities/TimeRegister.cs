﻿using System;

using PL.Domain.Core;
using PL.Domain.Core.Interfaces;

namespace PL.Domain.Entities {
	public class TimeRegister : GuidHandler, ISelfValidation {
		protected TimeRegister() { /* Private empty contructor */ }

		public TimeRegister(DateTime register, Guid developerId, Guid projectId, string projectName) {
			Id = NewId;
			Register = register;
			ProjectName = projectName;
			DeveloperId = developerId;
			ProjectId = projectId;
		}

		public Guid Id { get; private set; }
		public string ProjectName { get; private set; }
		public DateTime Register { get; private set; }
		public Guid DeveloperId { get; private set; }
		public Guid ProjectId { get; private set; }

		public virtual Developer Developer { get; private set; }
		public virtual Project Project { get; private set; }

		public ValidationResult ValidationResult { get; private set; }

		public TimeRegister Edit(DateTime register, Guid developerId, Guid projectId, string projectName) {
			Register = register;
			ProjectName = projectName;
			DeveloperId = developerId;
			ProjectId = projectId;
			return this;
		}

		public bool IsValid => true; // TODO criar as validações
	}
}
