﻿using PL.Domain.Core;
using PL.Domain.Core.Interfaces;

using System;

namespace PL.Domain.Entities {
	/// <summary> Defines the participation of a developer in a project. </summary>
	public class DeveloperProject : GuidHandler, ISelfValidation {
		protected DeveloperProject() { /* Empty private constructor */ }

		public DeveloperProject(Guid developerId) {
			Id = NewId;
			DeveloperId = developerId;
		}

		public Guid Id { get; private set; }

		public Guid DeveloperId { get; private set; }
		public Guid ProjectId { get; private set; }

		public DateTime StartPeriod { get; private set; }
		public DateTime EndPeriod { get; private set; }

		public virtual Developer Developer { get; private set; }
		public virtual Project Project { get; private set; }

		public ValidationResult ValidationResult { get; private set; }

		public DeveloperProject SetPeriod(DateTime startPeriod, DateTime endPeriod) {
			StartPeriod = startPeriod;
			EndPeriod = endPeriod;
			return this;
		}

		public DeveloperProject SetDeveloper(Guid developerId) {
			DeveloperId = developerId;
			return this;
		}
		public bool IsValid => true; // TODO criar as validações
	}
}
