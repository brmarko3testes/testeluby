﻿
using System;

namespace PL.Domain.DTO {
	/// <summary> Defines the participation of a developer in a project. </summary>
	public class DeveloperProjectDTO {
		public Guid Id { get; private set; }

		public Guid ProjectId { get; private set; }
		public Guid DeveloperId { get; private set; }

		public DateTime StartPeriod { get; private set; }
		public DateTime EndPeriod { get; private set; }
		public int Total { get; private set; }
	}
}
