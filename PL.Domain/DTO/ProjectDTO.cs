﻿using System;
using System.Collections.Generic;

using PL.Domain.Entities;

namespace PL.Domain.DTO {

	public class ProjectDTO {
		public Guid Id { get; set; }

		public string Name { get; set; }

		public DateTime StartDate { get; private set; }
		public DateTime EndDate { get; private set; }

		public int Total { get; set; }
	}
}
