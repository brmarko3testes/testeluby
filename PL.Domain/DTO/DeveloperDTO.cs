﻿using System;

namespace PL.Domain.DTO {
	public class DeveloperDTO {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string CPF { get; set; }
		public int Total { get; set; }
	}
}
