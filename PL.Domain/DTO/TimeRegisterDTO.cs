﻿
using System;

namespace PL.Domain.DTO {
	/// <summary> Defines the participation of a developer in a project. </summary>
	public class TimeRegisterDTO {
		public Guid Id { get; private set; }

		public string ProjectName { get; private set; }
		public DateTime Register { get; private set; }

		public Guid ProjectId { get; private set; }
		public Guid DeveloperId { get; private set; }

		public int Total { get; private set; }
	}
}
