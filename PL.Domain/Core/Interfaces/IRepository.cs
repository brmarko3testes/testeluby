﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PL.Domain.Core.Interfaces
{
	public interface IRepository<TEntity> : IDisposable where TEntity : class
	{
		void Add(TEntity entity);
		void Update(TEntity entity);
		void Delete(TEntity entity);
		TEntity GetById(Guid id);
		Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate);
	}
}
