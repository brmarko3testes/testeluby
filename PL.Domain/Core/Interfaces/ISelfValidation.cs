﻿namespace PL.Domain.Core.Interfaces {
	public interface ISelfValidation {
		ValidationResult ValidationResult { get; }

		bool IsValid { get; }
	}
}
