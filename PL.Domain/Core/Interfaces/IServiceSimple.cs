﻿using System;

namespace PL.Domain.Core.Interfaces {
	public interface IServiceSimple<TEntity> where TEntity : class {
		ValidationResult Add(TEntity entity);
		ValidationResult Update(TEntity entity);
		ValidationResult Delete(TEntity entity);
		TEntity GetById(Guid id);
	}
}
