﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PL.Domain.Core.Pagination
{
	public class Paged<T> : IPaged<T>
	{
		private readonly IQueryable<T> source;

		public Paged(IQueryable<T> source) =>
			this.source = source;

		public IEnumerator<T> GetEnumerator() =>
			source.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() =>
			GetEnumerator();

		public int Count => source.Count();

		public IEnumerable<T> GetRange(int skip, int take)
		{
			if (skip == 0 && take == 0)
				return source.ToList();
			else
				return source.Skip(skip).Take(take).ToList();
		}

		public IPagedResult<T> GetPaged(int skip, int take) =>
		new PagedResult<T> {
			recordsTotal = Count,
			data = GetRange(skip, take)
		};

		// TODO finish this
		public IPagedResult<T> GetDataReader() =>
			throw new NotImplementedException();
	}
}
