﻿using System.Collections.Generic;

namespace PL.Domain.Core.Pagination
{
	public class PagedResult<T> : IPagedResult<T>
	{
		public int recordsTotal { get; set; }
		public int recordsFiltered { get; set; }
		public IEnumerable<T> data { get; set; }
	}
}
