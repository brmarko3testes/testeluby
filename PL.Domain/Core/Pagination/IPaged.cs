﻿using System.Collections.Generic;

namespace PL.Domain.Core.Pagination
{
    public interface IPaged<T> : IEnumerable<T>
    {
        int Count { get; }

        IEnumerable<T> GetRange(int skip, int take);

        IPagedResult<T> GetPaged(int skip, int take);

        IPagedResult<T> GetDataReader();
    }
}
