﻿using System.Collections.Generic;

namespace PL.Domain.Core.Pagination
{
    public interface IPagedResult<T>
    {
        int recordsTotal { get; set; }
        int recordsFiltered { get; set; }
        IEnumerable<T> data { get; set; }
    }
}
