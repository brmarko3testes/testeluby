﻿namespace PL.Domain.Core.Pagination.Datatable
{
    public class DataOrder
    {
        public int Column { get; set; }
        public string Dir { get; set; }
    }
}
