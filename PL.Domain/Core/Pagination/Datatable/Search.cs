﻿namespace PL.Domain.Core.Pagination.Datatable
{
    public class Search
    {
        public bool Regex { get; set; }
        public string Value { get; set; }
    }
}
