﻿using System.Collections.Generic;

namespace PL.Domain.Core.Pagination.Datatable
{
    public class DatatableParameter
    {
		public DatatableParameter() {
            Columns = new List<DataTableColumn>();
            Order = new List<DataOrder>();
		}

        // Usado para definir-se quais colunas devem ser resgatadas do banco:
        public List<DataTableColumn> Columns { get; set; }

        // Usado para ordenar a lista:
        public List<DataOrder> Order { get; set; }

        // Usado para buscas:
        public Search Search { get; set; }

        // Usado como parâmetros de paginação:
        public int Start { get; set; }
        public int Length { get; set; }

        public int Skip { get { return Start; } }
        public int Take { get { return Length; } }
    }
}
