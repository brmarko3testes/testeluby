﻿using System.Collections.Generic;
using System.Linq;

namespace PL.Domain.Core {
	public class ValidationResult {
		private readonly List<string> _errors;
		public List<string> Errors => _errors;
		public bool IsValid => !_errors.Any();
		public object Id { get; private set; }

		public ValidationResult() => _errors = new List<string>();

		public ValidationResult Add(string error) {
			_errors.Add(error);
			return this;
		}

		public ValidationResult Add(IList<string> errors) {
			if (errors == null)
				return this;

			_errors.AddRange(errors);
			return this;
		}

		public ValidationResult Add(IEnumerable<string> errors) {
			if (errors == null)
				return this;

			_errors.AddRange(errors);
			return this;
		}

		public ValidationResult Add(params ValidationResult[] validationResults) {
			if (validationResults == null)
				return this;

			var validations = validationResults.Where(w => w != null);
			foreach (var validation in validations)
				_errors.AddRange(validation.Errors);

			return this;
		}

		public void SetId(object id) => Id = id;

		public void ClearErros() => Errors?.Clear();
	}
}
