﻿using System;

namespace PL.Domain.Core
{
	public class GuidHandler
	{
		public GuidHandler(Guid? id = null) =>
			NewId = id ?? Guid.NewGuid();

		public Guid NewId { get; }
	}
}
