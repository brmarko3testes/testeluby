﻿using System;

using PL.Domain.Core.Interfaces;

namespace PL.Domain.Core {
	public class ServiceSimple<TEntity> : IServiceSimple<TEntity> where TEntity : class {
		private readonly IRepository<TEntity> _repo;

		protected ValidationResult ValidationResult { get; }

		public ServiceSimple(IRepository<TEntity> repo) {
			_repo = repo;
			ValidationResult = new ValidationResult();
		}

		public ValidationResult Add(TEntity entity) {
			if (!ValidationResult.IsValid)
				return ValidationResult;

			if (entity is ISelfValidation selfValidationEntity && !selfValidationEntity.IsValid)
				return selfValidationEntity.ValidationResult;

			_repo.Add(entity);
			return ValidationResult;
		}

		public ValidationResult Delete(TEntity entity) {
			if (!ValidationResult.IsValid)
				return ValidationResult;

			if (entity is ISelfValidation selfValidationEntity && !selfValidationEntity.IsValid)
				return selfValidationEntity.ValidationResult;

			_repo.Delete(entity);
			return ValidationResult;
		}

		public TEntity GetById(Guid id) =>
			_repo.GetById(id);

		public ValidationResult Update(TEntity entity) {
			if (!ValidationResult.IsValid)
				return ValidationResult;

			if (entity is ISelfValidation selfValidationEntity && !selfValidationEntity.IsValid)
				return selfValidationEntity.ValidationResult;

			_repo.Update(entity);
			return ValidationResult;
		}
	}
}
