﻿namespace PL.Domain.Core.Helpers {
	public class ModelHelper {
		public static class StringColumns {
			/// <summary> int: 11 </summary>
			public static int CPF_LENGHT = 11;

			/// <summary> int: 8 </summary>
			public static int XSMALL_LENGTH = 8;

			/// <summary> int: 30 </summary>
			public static int SMALL_LENGTH = 30;

			/// <summary> int: 60 </summary>
			public static int MEDIUM_LENGTH = 60;

			/// <summary> int: 120 </summary>
			public static int LARGE_LENGTH = 120;

			/// <summary> int: 500 </summary>
			public static int TEXT_AREA_LENGTH = 500;
		}
	}
}