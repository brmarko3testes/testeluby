﻿using System;
using System.Threading.Tasks;

using PL.Domain.Core.Interfaces;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;
using PL.Domain.Entities;

namespace PL.Domain.Interfaces.Services {
	public interface IDeveloperProjectService : IServiceSimple<DeveloperProject> {

		Task<IPagedResult<DeveloperProjectDTO>> ListByProjectId(Guid projectId);
		Task<IPagedResult<DeveloperProjectDTO>> ListByDeveloperId(Guid projectId);
		bool ExistsByName(string name);
		DeveloperProjectDTO GetByName(string name);
	}
}
