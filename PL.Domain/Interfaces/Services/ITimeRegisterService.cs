﻿using System;
using System.Threading.Tasks;

using PL.Domain.Core.Interfaces;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;
using PL.Domain.Entities;

namespace PL.Domain.Interfaces.Services {
	public interface ITimeRegisterService : IServiceSimple<TimeRegister> {
		Task<IPagedResult<TimeRegisterDTO>> List(DatatableParameter parameter);
	}
}
