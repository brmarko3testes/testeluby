﻿using System.Threading.Tasks;

using PL.Domain.Core.Interfaces;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;
using PL.Domain.Entities;

namespace PL.Domain.Interfaces.Services {
	public interface IDeveloperService : IServiceSimple<Developer> {

		Task<IPagedResult<DeveloperDTO>> List(DatatableParameter parameter);

		bool ExistsByDocument(string cpf);

		DeveloperDTO GetByName(string nome);
	}
}
