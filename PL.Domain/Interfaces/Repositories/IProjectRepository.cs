﻿using PL.Domain.Core.Interfaces;
using PL.Domain.Entities;

namespace PL.Domain.Interfaces.Repositories {
	public interface IProjectRepository : IRepository<Project> { }
}
