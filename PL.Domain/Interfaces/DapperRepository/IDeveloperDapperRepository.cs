﻿using System.Threading.Tasks;

using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;

namespace PL.Domain.Interfaces.DapperRepository {
	public interface IDeveloperDapperRepository {
		bool ExistsByDocument(string cpf);
		Task<IPagedResult<DeveloperDTO>> ListAll(DatatableParameter parameter);
		DeveloperDTO GetByName(string name);
	}
}
