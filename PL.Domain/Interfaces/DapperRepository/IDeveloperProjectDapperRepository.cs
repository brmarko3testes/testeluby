﻿using System;
using System.Threading.Tasks;

using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;

namespace PL.Domain.Interfaces.DapperRepository {
	public interface IDeveloperProjectDapperRepository {
		Task<IPagedResult<DeveloperProjectDTO>> GetByDeveloper(Guid developerId);
		Task<IPagedResult<DeveloperProjectDTO>> GetByProject(Guid projectId);
		bool ExistsByName(string name);
		DeveloperProjectDTO GetByName(string name);
	}
}
