﻿using System.Threading.Tasks;

using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.DTO;

namespace PL.Domain.Interfaces.DapperRepository {
	public interface ITimeRegisterDapperRepository {
		Task<IPagedResult<TimeRegisterDTO>> ListAll(DatatableParameter parameter);
	}
}
