# Desafio Back-end Luby Software
Primeiramente, obrigado pelo seu interesse em trabalhar na Luby. Somos uma fábrica de software 
com mais de 110 desenvolvedores e 15 anos de mercado. Temos atuação em mais de 5 países e estamos 
em busca de talentos para integrar o nosso time no desenvolvimento .NET de forma 100% remota.

#### Premissas:
- Criar uma API usando .NET CORE.
- O banco de dados pode ser  MySql ou SQL Server.

#### Teste:
Desenvolver um serviço que seja capaz de gerar um lançamento de horas.
- Um lançamento de horas é composta por pelo menos **id**, **data inicio**, **data fim**, 
**desenvolvedor**.

#### Sua tarefa é desenvolver os serviços REST abaixo:
- CRUD para desenvolvedor (Será considerado um diferencial paginação na listagem)
- CRUD de projeto (Será considerado um diferencial paginação na listagem)
- Criar um lançamento de hora
- Retornar ranking dos 5 desenvolvedores da semana com maior média de horas trabalhadas.

#### Algumas regras à serem consideradas
- Um desenvolvedor só pode lançar horas em projetos que ele esteja vinculado
- Um desenvolvedor só pode lançar horas se estiver autenticado (Autenticação JWT com expiração 
de 5 minutos)
- Validações de integridade e duplicidade
- Antes de cadastrar um desenvolvedor, devemos validar se seu CPF é válido, para essa validação, 
pode ser usado o endpoint (https://run.mocky.io/v3/067108b3-77a4-400b-af07-2db3141e95c9)
- Na confirmação do lançamento de horas, uma notificação é enviada, e o serviço pode estar 
indisponível/instável. Para enviar a notificação, use o endpoint abaixo 
(https://run.mocky.io/v3/a1b59b8e-577d-4996-a4c5-56215907d9dd)

#### Instruções:
1. Realizar `fork` deste projeto.
2. Desenvolver em cima do seu `fork`.
3. Após finalizar, realizar o `pull request`.
4. Atualize esse README.md com sua identificação no fim do arquivo
5. Fique à vontade para perguntar qualquer dúvida aos recrutadores.

#### E por fim:
- Gostaríamos de ver o uso do controle de versão.
- Entendimento de OO, conceitos de SOLID, e outros relacionados
- Reuso do código
- Vamos avaliar a maneira que você escreveu seu código, a solução apresentada.
- Caso encontre algum impedimento no decorrer do desenvolvimento, entregue da maneira que preferir 
e faça uma explicação sobre o impedimento.
- Avaliaremos também sua postura, honestidade e a maneira que resolve problemas.

#### Desejável (Será considerado um diferencial)
- Automação de testes - unitários e integração.
- Configurar o Swagger para termos acesso a documentação da API.
- É de suma importância se utilizar das melhores práticas para um projeto seguro e organizado, 
como a utilização de controllers, services, factory, middlewares, controle de exceções, utilização 
de um ORM ou MicroORM (Object Relational Mapper) para operações de banco de dados.
- Criar um client WEB para consumir essa API.

#### Identificação:
Nome: Marco Marchiori
E-mail: brmarko3@gmail.com

#### Questions:
#1 - How was your start in the area? At what age did you start getting interested in technology? 
- I started developing with JAVA in my early high-school days. Always had the interest of understanding
how computers work and to make them work;

#2 - Why do you think that you are a good fit for this position? 
 - My skills are the fit for the position and I have interest in learn them further;

#3 - Can you tell us a little about your most challenging project? 
 - Well in my Freelancer time I have had the chance to encounter several challanging projects, most of them
 were connecting APIs and/or creating them;

#4 - Where do you work today? Do you like your job? What are the positive and negative points? 
 - I am still a freelancer looking for a full/part-time job;
 - I enjoy what enough to code for fun in my spare time;
 - The good in it is the fact that I'm always up-to-date with new technologies, nevertheless, the downside
 is the time management is getting hard to handdle, since I can't even see it pass when I'm  turning
 coffee into code;

#5 - Do you work from the home office? How do you manage your work time at home? Are you interested in
 working full-time home-office? 
 - Yes;
 - As Freelancer (Whenere I don't really need to attend to meetings or scrums, I have little-to-no time 
 management, I simply code untill I run out of coffee and that is one of the big reasons why I'm looking for
 a full/part-time job. This way I will have to force myself to discipline my sleeping/waking routine;

#6 -What are your questions about this work model?
 - Do I get a vale-café?
 Yes, that's really all I have to ask...

#7 - Do you have PJ or MEI? What is your reference hourly value today?
 - Not quite yet. sill something I should have made ages ago, but I'm totally down to making a PJ;

#8 - Where do you currently live? 
- Curitiba, two blocks away from Palladium Mall (Guaíra/Portão);

#9 - What would be your availability to start in case of effectiveness?
- Yesterday.
I am currently holding no Freelancing projects precisely because I'm looking for a full/part-time job;

#### Notas:
Ocorreu um erro local quando fui fazer o fork do github e decidi fazer download do projecot como .zip
para não perder tempo, no entanto, ;e possível ver meu tratamento de versionamento em meu bitbucket de
testes para vagas de emprego:
https://bitbucket.org/brmarko3testes/