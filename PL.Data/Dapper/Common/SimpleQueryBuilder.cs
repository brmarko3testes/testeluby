﻿using System.Dynamic;
using System.Text;

namespace PL.Data.Dapper.Common {
	public class SimpleQueryBuilder {

		/// <summary> New empty query </summary>
		public SimpleQueryBuilder() {
			Query = new StringBuilder();
			Parameters = new ExpandoObject();
		}

		/// <summary> Starts with a part of a Query, a Parameter Name and the Parameter Value </summary>
		public SimpleQueryBuilder(string query, string paramName, string paramData) {
			Add(query);
			Add(paramName, paramData);
		}

		/// <summary> Starts with a Parameter Name and the Parameter Value </summary>
		public SimpleQueryBuilder(string paramName, string paramData) =>
			Add(paramName, paramData);

		/// <summary> Starts with a part of a Query </summary>
		public SimpleQueryBuilder(string query) =>
			Add(query);

		private StringBuilder Query { get; set; }
		private dynamic Parameters { get; set; }

		/// <summary> Adds a part of a Query, a Parameter Name and the Parameter Value </summary>
		public void Add(string query, string paramName, string paramData) {
			Add(query);
			Add(paramName, paramData);
		}

		/// <summary> Adds a part of a Query </summary>
		public void Add(string query) =>
			Query.AppendLine(query);

		/// <summary> Adds a Parameter Name and the Parameter Value </summary>
		public void Add(string paramName, string paramData) =>
			Parameters[paramName] = paramData;

		/// <summary> Gets only the Query as String </summary>
		public override string ToString() =>
			Query.ToString();

		/// <summary> Gets only the Parameters as Object </summary>
		public object Params() =>
			(object) Parameters;

	}
}
