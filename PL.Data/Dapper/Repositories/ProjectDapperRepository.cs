﻿using PL.Data.Dapper.Common;

using Dapper;
using PL.Domain.DTO;
using System.Linq;
using System.Threading.Tasks;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using System.Dynamic;
using System.Text;
using PL.Domain.Interfaces.DapperRepository;
using System.Collections.Generic;

namespace PL.Data.Dapper {
	public class ProjectDapperRepository : RepositoryConnection, IProjectDapperRepository {
		public async Task<IPagedResult<ProjectDTO>> ListAll(DatatableParameter parameter) {
			var query = new SimpleQueryBuilder("SELECT * COUNT(*) OVER() AS Total FROM Project");

			if (parameter.Search != null && !string.IsNullOrEmpty(parameter.Search.Value))
				query.Add("AND Nome LIKE @WhereLike", "WhereLike", $"%{parameter.Search.Value}%");

			if (parameter.Skip > 0)
				query.Add("OFFSET @Skip ROWS", "Skip", parameter.Skip.ToString());

			if (parameter.Take > 0)
				query.Add("FETCH NEXT @Take ROWS ONLY", "Take", parameter.Take.ToString());

			IEnumerable<ProjectDTO> list;

			using (var Ctx = LubbyRead)
				list = await Ctx.QueryAsync<ProjectDTO>(query.ToString(), query.Params());

			var total = list.Any() ? list.FirstOrDefault().Total : 0;

			return new PagedResult<ProjectDTO> {
				recordsTotal = total,
				recordsFiltered = total,
				data = list
			};
		}

		public ProjectDTO GetByName(string name) {
			var query = new SimpleQueryBuilder("SELECT * FROM Project WHERE UPPER(Name) = UPPER(@Name)", "Nome", name);

			using (var Ctx = LubbyRead)
				return Ctx.QueryFirstOrDefault<ProjectDTO>(query.ToString(), query.Params());
		}
	}
}
