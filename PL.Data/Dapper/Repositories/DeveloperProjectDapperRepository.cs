﻿using PL.Data.Dapper.Common;

using Dapper;
using PL.Domain.DTO;
using System.Linq;
using System.Threading.Tasks;
using PL.Domain.Core.Pagination;
using PL.Domain.Interfaces.DapperRepository;
using System;

namespace PL.Data.Dapper {
	public class DeveloperProjectDapperRepository : RepositoryConnection, IDeveloperProjectDapperRepository {
		public bool ExistsByName(string name) {
			var query = new SimpleQueryBuilder("SELECT Id FROM DeveloperProject WHERE UPPER(Name) = UPPER(@Name)", "Name", name);

			using (var Ctx = LubbyRead)
				return Ctx.Query<DeveloperProjectDTO>(query.ToString(), query.Params()).Any();
		}

		public Task<IPagedResult<DeveloperProjectDTO>> GetByDeveloper(Guid developerId) {
			using (var Ctx = LubbyRead) {
				// TODO get by dev
			}
			throw new NotImplementedException();
		}


		public DeveloperProjectDTO GetByName(string name) {
			var query = new SimpleQueryBuilder("SELECT * FROM Developer WHERE UPPER(Name) = UPPER(@Name)", "Name", name);
			using (var Ctx = LubbyRead)
				return Ctx.QueryFirstOrDefault<DeveloperProjectDTO>(query.ToString(), query.Params());
		}

		public Task<IPagedResult<DeveloperProjectDTO>> GetByProject(Guid projectId) {
			using (var Ctx = LubbyRead) {
				// TODO get by proj
			}
			throw new NotImplementedException();
		}
	}
}
