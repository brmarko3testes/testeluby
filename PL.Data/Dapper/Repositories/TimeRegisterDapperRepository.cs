﻿using PL.Data.Dapper.Common;

using Dapper;
using PL.Domain.DTO;
using System.Threading.Tasks;
using PL.Domain.Interfaces.DapperRepository;
using System;
using System.Collections.Generic;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using System.Linq;

namespace PL.Data.Dapper {
	public class TimeRegisterDapperRepository : RepositoryConnection, ITimeRegisterDapperRepository {
		public async Task<IPagedResult<TimeRegisterDTO>> ListAll(DatatableParameter parameter) {
			var query = new SimpleQueryBuilder("SELECT * COUNT(*) OVER() AS Total FROM TimeRegister");

			if (parameter.Search != null && !string.IsNullOrEmpty(parameter.Search.Value))
				query.Add("AND ProjectName LIKE @WhereLike", "WhereLike", $"%{parameter.Search.Value}%");

			if (parameter.Skip > 0)
				query.Add("OFFSET @Skip ROWS", "Skip", parameter.Skip.ToString());

			if (parameter.Take > 0)
				query.Add("FETCH NEXT @Take ROWS ONLY", "Take", parameter.Take.ToString());

			IEnumerable<TimeRegisterDTO> list;

			using (var Ctx = LubbyRead)
				list = await Ctx.QueryAsync<TimeRegisterDTO>(query.ToString(), query.Params());

			var total = list.Any() ? list.FirstOrDefault().Total : 0;

			return new PagedResult<TimeRegisterDTO> {
				recordsTotal = total,
				recordsFiltered = total,
				data = list
			};
		}
	}
}
