﻿using PL.Data.Dapper.Common;

using Dapper;
using PL.Domain.DTO;
using System.Linq;
using System.Threading.Tasks;
using PL.Domain.Core.Pagination;
using PL.Domain.Core.Pagination.Datatable;
using PL.Domain.Interfaces.DapperRepository;
using System.Collections.Generic;

namespace PL.Data.Dapper {
	public class DeveloperDapperRepository : RepositoryConnection, IDeveloperDapperRepository {
		public bool ExistsByDocument(string cpf) {
			var query = new SimpleQueryBuilder($@"SELECT Id FROM Developer WHERE Cpf = @DocNumber", "DocNumber", cpf);

			using (var Ctx = LubbyRead)
				return Ctx.Query<DeveloperDTO>(query.ToString(), query.Params()).Any();
		}

		public async Task<IPagedResult<DeveloperDTO>> ListAll(DatatableParameter parameter) {
			var query = new SimpleQueryBuilder("SELECT * COUNT(*) OVER() AS Total");
			query.Add("FROM Developer");

			if (parameter.Search != null && !string.IsNullOrEmpty(parameter.Search.Value))
				query.Add("AND Nome LIKE @WhereLike", "WhereLike", $"%{parameter.Search.Value}%");

			var orderColumn = parameter.Order.FirstOrDefault();
			switch (orderColumn.Column) {
				case 1:
					query.Add($"ORDER BY CPF {orderColumn.Dir}");
					break;
				default:
					query.Add($"ORDER BY Name {orderColumn.Dir}");
					break;
			}

			if (parameter.Skip > 0)
				query.Add("OFFSET @Skip ROWS", "Skip", parameter.Skip.ToString());

			if (parameter.Take > 0)
				query.Add("FETCH NEXT @Take ROWS ONLY", "Take", parameter.Take.ToString());

			IEnumerable<DeveloperDTO> list;

			using (var Ctx = LubbyRead)
				list = await Ctx.QueryAsync<DeveloperDTO>(query.ToString(), query.Params());

			var total = list.Any() ? list.FirstOrDefault().Total : 0;

			return new PagedResult<DeveloperDTO> {
				recordsTotal = total,
				recordsFiltered = total,
				data = list
			};
		}

		public DeveloperDTO GetByName(string name) {
			var query = new SimpleQueryBuilder("SELECT * FROM Paciente WHERE UPPER(Nome) = UPPER(@Nome)", "Name", name);

			using (var Ctx = LubbyRead)
				return Ctx.QueryFirstOrDefault<DeveloperDTO>(query.ToString(), query.Params());
		}
	}
}
