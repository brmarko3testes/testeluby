﻿using PL.Domain.Entities;
using PL.Domain.Core.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PL.Data.EntityFramework.Mapping {
	public class DeveloperMap : IEntityTypeConfiguration<Developer> {
		public void Configure(EntityTypeBuilder<Developer> builder) {
			builder.HasKey(p => p.Id);
			builder.Property(p => p.UserName).HasMaxLength(ModelHelper.StringColumns.MEDIUM_LENGTH);
			builder.Property(p => p.Password).HasMaxLength(ModelHelper.StringColumns.SMALL_LENGTH);
			builder.Property(p => p.CPF).HasMaxLength(ModelHelper.StringColumns.CPF_LENGHT);
		}
	}
}
