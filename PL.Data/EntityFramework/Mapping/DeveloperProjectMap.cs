﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using PL.Domain.Entities;

namespace PL.Data.EntityFramework.Mapping {
	public class DeveloperProjectMap : IEntityTypeConfiguration<DeveloperProject> {
		public void Configure(EntityTypeBuilder<DeveloperProject> builder) {
			builder.HasKey(c => new { c.ProjectId, c.DeveloperId });

			builder.HasOne(c => c.Project)
				.WithMany(e => e.DevelopersInProject)
				.HasForeignKey(c => c.ProjectId);

			builder.HasOne(c => c.Developer)
				.WithMany()
				.HasForeignKey(c => c.DeveloperId);
		}
	}
}
