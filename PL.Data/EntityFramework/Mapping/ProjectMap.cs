﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using PL.Domain.Core.Helpers;
using PL.Domain.Entities;

namespace PL.Data.EntityFramework.Mapping {
	public class ProjectMap : IEntityTypeConfiguration<Project> {
		public void Configure(EntityTypeBuilder<Project> builder) { 
			builder.HasKey(p => p.Id);
			builder.Property(p => p.Name).HasMaxLength(ModelHelper.StringColumns.MEDIUM_LENGTH);
		}

	}
}
