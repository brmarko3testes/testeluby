﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using PL.Domain.Entities;


namespace PL.Data.EntityFramework.Mapping {
	public class TimeRegisterMap : IEntityTypeConfiguration<TimeRegister> {
		public void Configure(EntityTypeBuilder<TimeRegister> builder) { 
			builder.HasKey(t => t.Id);

			builder.HasKey(c => new { c.ProjectId, c.DeveloperId });

			builder.HasOne(c => c.Developer)
				.WithMany()
				.HasForeignKey(c => c.DeveloperId);

		}

	}
}
