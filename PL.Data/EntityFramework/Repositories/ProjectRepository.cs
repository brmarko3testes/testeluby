﻿using PL.Data.EntityFramework.Repositories.Common;
using PL.Domain.Entities;
using PL.Domain.Interfaces.Repositories;

namespace PL.Data.EntityFramework.Repositories {
	public class ProjectRepository : GenericRepository<Project>, IProjectRepository { }
}
