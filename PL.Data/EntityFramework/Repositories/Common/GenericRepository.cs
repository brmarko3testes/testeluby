﻿using PL.Domain.Core.Interfaces;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace PL.Data.EntityFramework.Repositories.Common {
	public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class {

		protected readonly SQLContext WriteContext;
		protected readonly DbSet<TEntity> DbSet;
		public void Add(TEntity entity) =>
			DbSet.Add(entity);

		public void Update(TEntity entity) {
			var entry = WriteContext.Entry(entity);
			DbSet.Attach(entity);
			entry.State = EntityState.Modified;
		}

		public void Delete(TEntity entity) {
			WriteContext.Entry(entity).State = EntityState.Deleted;
			DbSet.Remove(entity);
			// Dependendo da situação, pode ser interessante criar uma coluna 
			// "active" na entidade e desativar ao invés de deletar.
		}

		public TEntity GetById(Guid id) =>
			DbSet.Find(id);

		public async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate) =>
			await DbSet.Where(predicate).AsNoTracking().FirstOrDefaultAsync();

		public void Dispose() {
			WriteContext.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}
