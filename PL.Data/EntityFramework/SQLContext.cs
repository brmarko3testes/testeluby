﻿using System.Configuration;

using Microsoft.EntityFrameworkCore;

using PL.Data.EntityFramework.Mapping;
using PL.Domain.Core;
using PL.Domain.Entities;

namespace PL.Data.EntityFramework {
	public class SQLContext : DbContext {
		public SQLContext() : base() { }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
			optionsBuilder.UseSqlServer("Data Source=WIN7-VM;Initial Catalog=LubyDB;Integrated Security=True");
			// TODO corrigir: new Strings().ConnectionString
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder) {

			modelBuilder.Ignore(typeof(ValidationResult));

			base.OnModelCreating(modelBuilder);

			modelBuilder // Configures the entities 4 the DB:
				.Entity<Developer>(new DeveloperMap().Configure)
				.Entity<Project>(new ProjectMap().Configure)
				.Entity<DeveloperProject>(new DeveloperProjectMap().Configure)
				.Entity<TimeRegister>(new TimeRegisterMap().Configure);
		}

		public DbSet<Developer> Developers { get; set; }
		public DbSet<Project> Projects { get; set; }
		public DbSet<DeveloperProject> DeveloperProject { get; set; }
		public DbSet<TimeRegister> TimeRegister { get; set; }
	}
}