﻿using System.Configuration;

namespace PL.Data {
	internal class Strings {
		public string ConnectionString => ConfigurationManager.ConnectionStrings["SqlServerConfig"].ConnectionString;
	}
}
