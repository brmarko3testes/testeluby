﻿using System;

using PL.Data.Dapper;
using PL.Data.EntityFramework;
using PL.Data.EntityFramework.Repositories;
using PL.Domain.Core;
using PL.Domain.Core.Interfaces;
using PL.Domain.Interfaces.DapperRepository;
using PL.Domain.Interfaces.Repositories;
using PL.Domain.Interfaces.Services;
using PL.Domain.Services;

using SimpleInjector;
using SimpleInjector.Integration.Web;

namespace PL.CrossCutting {
	public static class SimpleInjectorContainer {
		public static Container RegisterServices() {
			var container = new Container();

			container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

			container.Register<SQLContext>(Lifestyle.Scoped);
			container.Register(typeof(IServiceSimple<>), typeof(ServiceSimple<>));

			container.Register<IDeveloperService, DeveloperService>();
			container.Register<IDeveloperRepository, DeveloperRepository>();
			container.Register<IDeveloperDapperRepository, DeveloperDapperRepository>();

			container.Register<IProjectService, ProjectService>();
			container.Register<IProjectRepository, ProjectRepository>();
			container.Register<IProjectDapperRepository, ProjectDapperRepository>();

			return container;
		}
	}
}
